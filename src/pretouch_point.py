#!/usr/bin/env python
import numpy as np
import roslib
roslib.load_manifest('uw_pr2_haptics_teleop')
import rospy
import tf
from geometry_msgs.msg import PointStamped
from pr2_pretouch_msgs.msg import PretouchAcoustic

ROBOT_ORIGIN_FRAME = "0_link"
PRETOUCH_FRAME = "r_pretouch_tip_frame"
TOPIC_POINT = "pretouch_point"
CNT_THRESHOLD = 3
DISTANCE_THRESHOLD = 5 #(mm)

rospy.init_node("pretouch_point_node")

tf_listener = tf.TransformListener()
pub_point = rospy.Publisher(TOPIC_POINT, PointStamped)
point = PointStamped()
point.header.frame_id = ROBOT_ORIGIN_FRAME

cnt = 0

def addpoint(distance):
  '''
  distance (float): the distance between the object and sensor
  '''
  global point
  now = rospy.Time.now()
  tf_listener.waitForTransform(ROBOT_ORIGIN_FRAME, PRETOUCH_FRAME, now, rospy.Duration(0.5))
  (trans, rot) = tf_listener.lookupTransform(ROBOT_ORIGIN_FRAME, PRETOUCH_FRAME, now)

  # shift by 'distance' along the X-axis
  T = tf.transformations.translation_matrix([distance/1000, 0, 0])
  M = tf.transformations.quaternion_matrix(rot)
  M[0:3,3] = np.array([trans[0], trans[1], trans[2]])
  M = tf.transformations.concatenate_matrices(M, T)

  point.header.frame_id = ROBOT_ORIGIN_FRAME
  point.header.stamp = now
  point.point.x = M[0,3]
  point.point.y = M[1,3]
  point.point.z = M[2,3]
  pub_point.publish(point)

  rospy.loginfo("a pretouch point published at distance " + str(distance) + " mm")

def callback(msg):
  global cnt
  #print 'cnt=', cnt
  if msg.distance < DISTANCE_THRESHOLD:
    cnt += 1
  else:
    cnt = 0
  if cnt >= CNT_THRESHOLD:
    addpoint(msg.distance) if msg.distance > 1 else addpoint(1) 
    cnt = 0

sub_config = rospy.Subscriber("/pretouch", PretouchAcoustic, callback)
rospy.spin()

