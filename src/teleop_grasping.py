#!/usr/bin/env python
import roslib
roslib.load_manifest('uw_pr2_haptics_teleop')
import rospy
import actionlib
from std_msgs.msg import Bool
from pr2_controllers_msgs.msg import Pr2GripperCommandActionGoal, Pr2GripperCommandAction, Pr2GripperCommandGoal

SIDE = "r"
ACTION_SERVER = SIDE + "_gripper_controller/gripper_action"
BUTTON1_TOPIC = "/hapticdevice/button"
BUTTON2_TOPIC = "/hapticdevice/button2"

rospy.init_node('haptics_gripper_commander', anonymous=True)
gripper_client = actionlib.SimpleActionClient(ACTION_SERVER, Pr2GripperCommandAction)
gripper_client.wait_for_server()
button1_first = True
button2_first = True

BAD_STATES = [2,3,4,5,8]

def button2_callback(msg):
  '''
  Open the gripper
  '''
  global button1_first
  if msg.data and button1_first and gripper_client.get_result() not in BAD_STATES:
    rospy.loginfo("opening the" + SIDE + "gripper")
    button1_first = False
    goal = Pr2GripperCommandGoal()
    goal.command.position = 0.08
    goal.command.max_effort = -1.0
    gripper_client.send_goal(goal)
    gripper_client.wait_for_result(rospy.Duration.from_sec(5.0))
  elif not msg.data:
    button1_first = True

def button1_callback(msg):
  '''
  Close the gripper
  '''
  global button2_first
  if msg.data and button2_first and gripper_client.get_result() not in BAD_STATES:
    rospy.loginfo("closing the" + SIDE + "gripper")
    button2_first = False
    goal = Pr2GripperCommandGoal()
    goal.command.position = 0.0
    goal.command.max_effort = 40.0
    gripper_client.send_goal(goal)
    gripper_client.wait_for_result(rospy.Duration.from_sec(5.0))
  elif not msg.data:
    button2_first = True

rospy.Subscriber(BUTTON1_TOPIC, Bool, button1_callback)
rospy.Subscriber(BUTTON2_TOPIC, Bool, button2_callback)
rospy.spin()

