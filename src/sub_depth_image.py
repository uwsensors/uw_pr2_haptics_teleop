#!/usr/bin/env python
import struct
import roslib
roslib.load_manifest('uw_pr2_haptics_teleop')
import rospy
from sensor_msgs.msg import Image
from std_msgs.msg import Int32MultiArray

keep = []

pub = rospy.Publisher('/head_mount_kinect/depth_registered/image_rect_raw_filtered', Image)
nan = list(struct.pack('f', float('nan')))
zeros = list(struct.pack('h', 0))

def callback(depth_msg):
    #encoding=16UC1, width=640, height=480, step1280, size=307200
    #print "encoding=", depth_msg.encoding, ", width=" ,depth_msg.width, ", height=" , depth_msg.height , ", step" , depth_msg.step , ", size=" , len(depth_msg.data)

    #print [struct.unpack('f', depth_msg.data[i:i+4]) for i in range(0:4:len(depth_msg.data))]
    #print [ord(depth_msg.data[i]) for i in range(1000)]
    #print [struct.unpack('f', depth_msg.data[i:i+4]) for i in range(0,len(depth_msg.data),4)]
    #print depth_msg.data

    datalist = list(depth_msg.data)
    for i in keep:
      datalist[2*i:2*i+2] = zeros
    depth_msg.data = ''.join(datalist)
    pub.publish(depth_msg)
    '''
    datalist = list(depth_msg.data)
    #depth_msg.data[0] = '\x00'
    for i in keep:
      datalist[4*i:4*i+4] = nan
    depth_msg.data = ''.join(datalist)
    pub.publish(depth_msg)
    '''

def callback_keep(msg):
    global keep
    keep = msg.data
    #print len(msg.data)
    #print msg.data

def listener():
    rospy.init_node('listener', anonymous=True)
    #rospy.Subscriber("/head_mount_kinect/depth_registered/image_rect", Image, callback)
    rospy.Subscriber("/head_mount_kinect/depth_registered/image_rect_raw", Image, callback)
    rospy.Subscriber("keep_array", Int32MultiArray, callback_keep)
    rospy.spin()

if __name__ == '__main__':
    listener()
