#!/usr/bin/env python
import roslib
roslib.load_manifest('uw_pr2_haptics_teleop')
import rospy
import tf
from geometry_msgs.msg import PoseStamped, Pose
from std_msgs.msg import Float64MultiArray, Bool
import numpy as np

# Params
TOPIC_CONTROL = "r_cart/command_pose"
TOPIC_CONFIG = "hapticdevice/configuration"
TOPIC_BUTTON = "hapticdevice/button"
ROBOT_ORIGIN_FRAME = "torso_lift_link"
ROBOT_EFF_FRAME = "r_wrist_roll_link"
SCALE_FACTOR_X = 0.003
SCALE_FACTOR_Y = 0.003
SCALE_FACTOR_Z = 0.003

rospy.init_node("uw_pr2_haptics_teleop_node")

# Global vars
is_engaged = False
is_reset = False
pub_control = rospy.Publisher(TOPIC_CONTROL, PoseStamped)
ps = PoseStamped()
ps.header.frame_id = ROBOT_ORIGIN_FRAME
eef_ref_position = np.zeros(3) # [x,y,z]
eef_ref_orientation = np.zeros(4) # [x,y,z,w]
omni_ref_position = np.zeros(3)
omni_ref_rot_mat = np.matrix(np.eye(4))
tf_listener = tf.TransformListener()

def reset_origins(config_msg):
  global eef_ref_position, eef_ref_orientation, omni_ref_position, omni_ref_rot_mat
  # Record the current robot EEF pose
  try:
    now = rospy.Time.now()
    tf_listener.waitForTransform(ROBOT_ORIGIN_FRAME, ROBOT_EFF_FRAME, now, rospy.Duration(4.0))
    (trans, rot) = tf_listener.lookupTransform(ROBOT_ORIGIN_FRAME, ROBOT_EFF_FRAME, now)
  except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
    raise
  eef_ref_position = np.array(trans)
  eef_ref_orientation = np.array(rot)
  omni_ref_position = np.array(config_msg.data[0:3])
  for i in xrange(3):
    for j in xrange(3):
      # 4,5,6; 7,8,9; 10,11,12
      omni_ref_rot_mat[j,i] = config_msg.data[3+i*3+j]


def move(config_msg):
  global ps
  # Compute the relative translation on omni
  dx = config_msg.data[0] - omni_ref_position[0]
  dy = config_msg.data[1] - omni_ref_position[1]
  dz = config_msg.data[2] - omni_ref_position[2]
  # Compute the relative rotation on omni
  # omni_rot_mat_ref * rot_mat = omni_rot_mat
  # rot_mat = omni_rot_mat_ref^(-1) * omni_rot_mat
  omni_rot_mat = np.matrix(np.eye(4))
  for i in xrange(3):
    for j in xrange(3):
      omni_rot_mat[j,i] = config_msg.data[3+i*3+j]
  rot_mat = omni_ref_rot_mat.I * omni_rot_mat
  #rot_mat =  omni_rot_mat
  rot_q = tf.transformations.quaternion_from_matrix(rot_mat)
  # Convert to the relative robot eef pose (scale factor)
  dx *= SCALE_FACTOR_X
  dy *= SCALE_FACTOR_Y
  dz *= SCALE_FACTOR_Z
  # Compute absolute robot eef position
  ps.pose.position.x = eef_ref_position[0] - dx
  ps.pose.position.y = eef_ref_position[1] - dy
  ps.pose.position.z = eef_ref_position[2] + dz
  # Compute absolute robot eef orientation
  q_new = tf.transformations.quaternion_multiply(rot_q, eef_ref_orientation)
  #q_new = rot_q
  ps.pose.orientation.x = q_new[0]
  ps.pose.orientation.y = q_new[1]
  ps.pose.orientation.z = q_new[2]
  ps.pose.orientation.w = q_new[3]
  # Send the command
  ps.header.stamp = rospy.Time.now()
  pub_control.publish(ps)


def callback_config(msg):
  global is_engaged, is_reset
  if is_engaged:
    if not is_reset:
      reset_origins(msg)
      is_reset = True
    move(msg)

def callback_button(msg):
  global is_engaged, is_reset
  if msg.data:
    "holding button .."
    # The user is holding the button
    if not is_engaged:
      # Change the state to engaged (only the first msg)
      is_engaged = True
      is_reset = False
      rospy.loginfo("Engaged")
  else:
    "The user just released the button"
    rospy.loginfo("Released")
    is_engaged = False
      

sub_config = rospy.Subscriber(TOPIC_CONFIG, Float64MultiArray, callback_config)
sub_button = rospy.Subscriber(TOPIC_BUTTON, Bool, callback_button)
rospy.spin()

