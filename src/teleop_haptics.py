#!/usr/bin/env python
import math
import numpy as np
import roslib
roslib.load_manifest('uw_pr2_haptics_teleop')
import rospy
import tf
from geometry_msgs.msg import PoseStamped, Pose, Quaternion, Point
from visualization_msgs.msg import Marker
import convert_functions

# Params
TOPIC_CONTROL = "r_cart/command_pose"
TOPIC_MARKER = "raven_forcerendering/visualization_marker_r"
TOPIC_BUTTON = "hapticdevice/button"
ROBOT_ORIGIN_FRAME = "0_link"
ROBOT_EFF_FRAME = "r_pretouch_frame"

rospy.init_node("uw_pr2_haptics_teleop_node")

# Global vars
pub_control = rospy.Publisher(TOPIC_CONTROL, PoseStamped)
#ps.header.frame_id = ROBOT_ORIGIN_FRAME
tf_listener = tf.TransformListener()
'''
tf_listener.waitForTransform(ROBOT_ORIGIN_FRAME, ROBOT_EFF_FRAME, rospy.Time(), rospy.Duration(4.0))
(trans,rot) = tf_listener.lookupTransform(ROBOT_ORIGIN_FRAME, ROBOT_EFF_FRAME, rospy.Time(0))
q = np.array(rot)
print "first q=",q
'''
# Make it parrallel to the table
# Get the pose w.r.t. a horizontal frame (base_link)
tf_listener.waitForTransform("base_link", "r_gripper_tool_frame", rospy.Time(), rospy.Duration(4.0))
(trans, rot) = tf_listener.lookupTransform("base_link", "r_gripper_tool_frame", rospy.Time(0))
print "rot=", rot
# get RPY, and make P (pitch) to zero
(r, p, y) = tf.transformations.euler_from_quaternion(rot, axes='sxyz')
print "(r, p, y)=", (r, p, y)
#rot = tf.transformations.quaternion_from_euler(r, 0, y, axes='sxyz')
rot = tf.transformations.quaternion_from_euler(0, 0, y, axes='sxyz')
#q = np.array(rot)
(r, p, y) = tf.transformations.euler_from_quaternion(rot, axes='sxyz')
print "(r, p, y)=", (r, p, y)
print "rot=", rot
ps = PoseStamped()
ps.header.frame_id = "base_link"
ps.header.stamp = rospy.Time.now()
ps.pose = Pose(Point(*trans), Quaternion(*rot))

# transform to ROBOT_ORIGIN_FRAME
tf_listener.waitForTransform("base_link", ROBOT_ORIGIN_FRAME, rospy.Time(), rospy.Duration(4.0))
ps_tmp = convert_functions.lists_to_pose_stamped(tf_listener, trans, rot, "base_link", ROBOT_ORIGIN_FRAME)
rot_tmp = [ps_tmp.pose.orientation.x, ps_tmp.pose.orientation.y, \
               ps_tmp.pose.orientation.z, ps_tmp.pose.orientation.w]
q = np.array(rot_tmp)


print ps
for i in range(100):
  pub_control.publish(ps)
  rospy.sleep(0.01)


def callback_marker(msg):
  global q
  # A geometry_msgs/Point for ROBOT_EFF_FRAME w.r.t /0_link

  if msg.type == msg.SPHERE:
    #if msg.points[0].x != 0 and msg.points[0].y != 0:
    if msg.pose.position.x != 0 and msg.pose.position.y != 0:
  
      ps.header.frame_id = ROBOT_ORIGIN_FRAME
      # This is the marker position is w.r.t to     
      ps.pose.position.x = msg.pose.position.x
      ps.pose.position.y = msg.pose.position.y
      ps.pose.position.z = msg.pose.position.z

      # msg.pose.orientation is the delta quaternion
      del_q = np.array([msg.pose.orientation.x, msg.pose.orientation.y, msg.pose.orientation.z, msg.pose.orientation.w])
      q = tf.transformations.quaternion_multiply(del_q, q)      
      #print "q=",q
      #print "del_q=", del_q
      ps.pose.orientation.x = q[0]
      ps.pose.orientation.y = q[1]
      ps.pose.orientation.z = q[2]
      ps.pose.orientation.w = q[3]

      ps.header = msg.header
      #print "Before transform:"
      #print ps

      # Transform to r_gripper_tool_frame
      #T = tf.transformations.translation_matrix([-0.040, 0.010, 0.000])
      T = tf.transformations.translation_matrix([-0.013, 0.010, 0.000])
      M = tf.transformations.quaternion_matrix(q)
      M[0:3,3] = np.array([ps.pose.position.x, ps.pose.position.y, ps.pose.position.z])
      M = tf.transformations.concatenate_matrices(M, T)
      
      ps.pose.position.x = M[0,3]
      ps.pose.position.y = M[1,3]
      ps.pose.position.z = M[2,3]

      pub_control.publish(ps)
      #print "After transform:"
      #print ps

sub_config = rospy.Subscriber(TOPIC_MARKER, Marker, callback_marker)
rospy.spin()

