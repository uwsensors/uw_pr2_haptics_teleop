import rospy
import roslib
roslib.load_manifest('uw_pr2_haptics_teleop')
from geometry_msgs.msg import PoseStamped

TOPIC_NAME = "r_cart/command_pose"

rospy.init_node("move_once")
pose_pub = rospy.Publisher(TOPIC_NAME, PoseStamped)
ps = PoseStamped()
ps.header.frame_id = "torso_lift_link"
ps.pose.position.x = 0.652647970878
ps.pose.position.y = -0.395153569956
ps.pose.position.z = 0
ps.pose.orientation.x = -0.776058683067
ps.pose.orientation.y = -0.194639653186
ps.pose.orientation.z = 0.387670552224
ps.pose.orientation.w = 0.457777095083


while not rospy.is_shutdown():
  rospy.loginfo("Publishing Pose ...")
  ps.pose.position.x -= 0.005
  ps.header.stamp = rospy.Time.now()
  pose_pub.publish(ps)
  rospy.sleep(0.2)
  #ps.pose.position.x += 0.005
