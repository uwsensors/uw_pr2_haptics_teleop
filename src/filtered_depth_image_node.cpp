// Filter out robot itself from the depth image
// @Liang-Ting Jiang 02/02/2014
#include "ros/ros.h"
#include "sensor_msgs/Image.h"
#include "std_msgs/Int32MultiArray.h"
#include <vector>

std::vector<int> keep;
ros::Publisher image_pub;

void depthImageCallback(const sensor_msgs::Image::Ptr& msg)
{
  for (unsigned int i = 0; i < keep.size(); ++i) {
    int idx = keep[i];
    msg->data[2*idx] = 0;
    msg->data[2*idx+1] = 0;
  }
  image_pub.publish(*msg);
}


void keepArrayCallback(const std_msgs::Int32MultiArray::ConstPtr& msg)
{
  keep.clear();
  for (unsigned int i = 0; i < msg->data.size(); ++i) {
    keep.push_back(msg->data[i]);
  }
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "filtered_depth_image_node");
  ros::NodeHandle n;
  std::string image_in, image_out, image_mask;
  n.param<std::string> ("image_in", image_in, "/head_mount_kinect/depth_registered/image_rect_raw");
  n.param<std::string> ("image_out", image_out, "/head_mount_kinect/depth_registered/image_rect_raw_filtered");
  n.param<std::string> ("image_mask", image_mask, "/keep_array");
  image_pub = n.advertise<sensor_msgs::Image>(image_out, 1);
  ros::Subscriber image_sub = n.subscribe(image_in, 1, depthImageCallback);
  ros::Subscriber keep_sub = n.subscribe(image_mask, 1, keepArrayCallback);
  ROS_INFO("filtered depth image published at %s", image_out.c_str());
  ros::spin();
  return 0;
}



